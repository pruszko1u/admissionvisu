'use strict';

window.chartColors = {
	red: 'rgb(255, 99, 132)',
	orange: 'rgb(255, 159, 64)',
	yellow: 'rgb(255, 205, 86)',
	green: 'rgb(75, 192, 192)',
	blue: 'rgb(54, 162, 235)',
	purple: 'rgb(153, 102, 255)',
	grey: 'rgb(201, 203, 207)'
};

window.datas = [];

window.dataToDisplay = [];
window.labels = [];

window.tri = [];
window.triIndex = [];

window.backgroundColor = [];

window.mainData = 2011;
window.indexMainData = 0;

window.config = {
	type: 'doughnut',
	data: {
		datasets: [{
			data: [],
			backgroundColor: window.backgroundColor,
			label: 'Année 2011'
		}],
		labels: window.labels
	},
	options: {
		responsive: true,
		legend: {
			position: 'top',
		},
		title: {
			display: true,
			text: 'Année 2011'
		},
		animation: {
			animateScale: true,
			animateRotate: true
		}
	}
};

function displayYear (yearIndex, year) {
	window.config.data.datasets[0].data = window.dataToDisplay[yearIndex];
	window.config.data.datasets[0].label = "Année " + year;
	window.config.options.title.text = "Année " + year;
	window.mainData = year;
	window.indexMainData = yearIndex;

	for (var i = 0; i < tri.length; i++) {
		window.config.data.datasets[0].label += " / " + tri[i]; 
		window.config.options.title.text += " / " + tri[i];
	}
};

function processData(allText) {
    var allTextLines = allText.split(/\r\n|\n/);
    var headers = allTextLines[0].split(';');
    var lines = [];

    for (var i=1; i<allTextLines.length; i++) {

        var data = allTextLines[i].split(';');

        if (data.length == headers.length) {
            var tarr = [];
            for (var j=1; j<headers.length; j++) {
                tarr.push(data[j]);
            }

            lines.push(tarr);
        }
    } 

    return lines;
}

function compareDataSets(boxChecked) {

	window.config.options.title.text = "Année " + window.mainData;

	window.tri = [];
	config.data.datasets.splice(1, config.data.datasets.length);

	if(boxChecked.length > 0) {
		for (var i = 0; i < boxChecked.length; i++) {
			var index = -1;
			if (boxChecked[i].value == 2011)
				index = 0;
			else if (boxChecked[i].value == 2012)
				index = 1;
			else if (boxChecked[i].value == 2013)
				index = 2;
			else if (boxChecked[i].value == 2014)
				index = 3;
			else if (boxChecked[i].value == 2015)
				index = 4;

			tri.push(boxChecked[i].value);
			triIndex.push(index);
			window.addDataSet(index, boxChecked[i].value);
		}
	} else {
		window.myDoughnut.update();
	}
}

function compareData(boxChecked) {
			
		window.dataToDisplay = [];
		window.labels = [];
		window.backgroundColor = [];

		if (config.data.datasets.length > 0) {

			for (var k = 0; k < boxChecked.length; k++) {
				if (boxChecked[k].value == 1) {
					window.backgroundColor.push(window.chartColors.red);
					window.labels.push('Inscrits');
				} else if (boxChecked[k].value == 2) {
					window.backgroundColor.push(window.chartColors.orange);
					window.labels.push('Admis S5');
				} else if (boxChecked[k].value == 3) {
					window.backgroundColor.push(window.chartColors.yellow);
					window.labels.push('Admis L3');
				} else if (boxChecked[k].value == 4) {
					window.backgroundColor.push(window.chartColors.green);
					window.labels.push('Inscrits IUT');
				} else if (boxChecked[k].value == 5) {
					window.backgroundColor.push(window.chartColors.blue);
					window.labels.push('Admis S5 IUT');
				} else if (boxChecked[k].value == 6) {
					window.backgroundColor.push(window.chartColors.purple);
					window.labels.push('Admis L3 IUT');
				} 
			}

			var dataTabTemp = [];

			for (var l = 0; l < window.datas.length; l++) {
				var dataTemp = [];
				for (var j = 0; j < boxChecked.length; j++) {
					dataTemp.push(window.datas[l][boxChecked[j].value - 1]);
				}

				dataTabTemp.push(dataTemp);
			}

			window.dataToDisplay = dataTabTemp;

			var tempDataSets = [];

			/* MAIN DATA */
			var newMainDataset = {
					backgroundColor: window.backgroundColor,			
					data: dataToDisplay[window.indexMainData],
					label: "Admissions " + window.mainData,
				};

				tempDataSets.push(newMainDataset);

			console.log("tri : " + window.tri);

			for (var i = 0; i < window.tri.length; i++) {
				console.log("compare");
				console.log(window.backgroundColor);

				console.log(dataToDisplay[triIndex[i]]);

				var newDataset = {
					backgroundColor: window.backgroundColor,			
					data: dataToDisplay[triIndex[i]],
					label: "Admissions " + window.tri[i],
				};

				tempDataSets.push(newDataset);
		}
			config.data.datasets = tempDataSets;
			config.data.labels = labels;

		}

		window.myDoughnut.update();
}

function isInArray(value, array) {
  return array.indexOf(value) > -1;
}

function addDataSet(index, year) {
	var newDataset = {
		backgroundColor: window.backgroundColor,
		data: window.dataToDisplay[index],
		label: 'Année ' + year,
	};

	window.config.options.title.text = window.config.options.title.text + " / " + year;

	config.data.datasets.push(newDataset);
	window.myDoughnut.update();
}