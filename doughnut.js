var datas;

		window.onload = function() {

			$.ajax({
        		type: "GET",
        		url: "statistiques.txt",
        		dataType: "text",
        		success: function(data) {
        			window.datas = processData(data);
        			
        			window.displayYear(0, 2011);

					var ctx = document.getElementById('chart-area').getContext('2d');
					window.myDoughnut = new Chart(ctx, window.config);
        		}
     		});
		};

		var colorNames = Object.keys(window.chartColors);

		document.getElementById('display2011').addEventListener('click', function() {
			window.displayYear(0, 2011);
			window.myDoughnut.update();
		});

		document.getElementById('display2012').addEventListener('click', function() {
			window.displayYear(1, 2012);
			window.myDoughnut.update();
		});

		document.getElementById('display2013').addEventListener('click', function() {
			window.displayYear(2, 2013);
			window.myDoughnut.update();
		});

		document.getElementById('display2014').addEventListener('click', function() {
			window.displayYear(3, 2014);
			window.myDoughnut.update();
		});

		document.getElementById('display2015').addEventListener('click', function() {
			window.displayYear(4, 2015);
			window.myDoughnut.update();
		});

		document.getElementById('submitCheckboxes').addEventListener('click', function() {
			var boxChecked = $('input[name="annee"]:checked');
			window.compareDataSets(boxChecked);
		});

		document.getElementById('submitDataCheckboxes').addEventListener('click', function() {
			var boxChecked = $('input[name="data"]:checked');
			window.compareData(boxChecked);
		});